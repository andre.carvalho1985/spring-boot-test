package br.com.trusthub.resources.exception;

import java.io.Serializable;

public class DefaultError implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String message;
	private Integer status;
	
	public DefaultError(Integer status, String message) {
		this.status = status;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
