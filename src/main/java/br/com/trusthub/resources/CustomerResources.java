package br.com.trusthub.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.trusthub.domain.Customer;
import br.com.trusthub.domain.enums.CreditRisk;
import br.com.trusthub.dto.CustomerDTO;
import br.com.trusthub.services.CustomerService;

@RestController
@RequestMapping("/customers")
public class CustomerResources {
	
	@Autowired
	private CustomerService customerService;
	
	@GetMapping
	public ResponseEntity<List<Customer>> findAll(){
		List<Customer> customers = customerService.findAll();
		return ResponseEntity.ok().body(customers);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Customer> findById(@PathVariable Integer id) {
		return ResponseEntity.ok().body(customerService.findById(id));
	}
	
	@PostMapping
	public ResponseEntity<Void> createCustomer(@Valid @RequestBody CustomerDTO customerDTO) {
		Customer customer = customerService.customerDTO(customerDTO);
		customer = customerService.createCustomer(customer);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(customer.getId()).toUri();
		
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Customer> updateCustomer(@PathVariable Integer id, @RequestBody CustomerDTO customerDTO) {
		Customer customer = new Customer(customerDTO.getName(), customerDTO.getCreditLimit(), CreditRisk.valueOf(customerDTO.getCreditRisk()));
		customer.setId(id);
		customerService.updateCustomer(customer);
		return ResponseEntity.noContent().build();
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteCustomer(@PathVariable Integer id) {
		customerService.deleteCustomer(id);
		return ResponseEntity.noContent().build();
	}
}
