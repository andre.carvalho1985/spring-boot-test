package br.com.trusthub.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CustomerDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	@NotBlank(message="Nome: Preenchimento obrigatório")
	@Size(min=3, message="Nome: Mínimo 3 caracteres")
	private String name;
	
	@NotNull(message="Limite de Crédito: Preenchimento obrigatório")
	private Double creditLimit;
	
	
	@NotBlank(message="Tipo de Risco: Preenchimento obrigatório")
	@Pattern(regexp="(A|B|C)", message="Tipo de Risco não cadastrado")
	private String creditRisk;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(Double creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCreditRisk() {
		return creditRisk;
	}

	public void setCreditRisk(String creditRisk) {
		this.creditRisk = creditRisk;
	}
	
}
