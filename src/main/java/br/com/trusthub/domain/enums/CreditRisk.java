package br.com.trusthub.domain.enums;

public enum CreditRisk {

	A(0), B(10), C(20);
	
	private Integer interestRate;
	
	private CreditRisk(Integer interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Integer interestRate) {
		this.interestRate = interestRate;
	}
	
}
