package br.com.trusthub.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import br.com.trusthub.domain.enums.CreditRisk;

@Entity
public class Customer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Enumerated(EnumType.STRING)
	private CreditRisk creditRisk;	
	
	private String name;
	private Double creditLimit;
	private Integer interestRate;
	
	public Customer() {	}
	
	public Customer(String name, Double creditLimit, CreditRisk creditRisk) {
		this.name = name;
		this.creditLimit = creditLimit;
		this.creditRisk = creditRisk;
		this.interestRate = creditRisk.getInterestRate();
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public CreditRisk getCreditRisk() {
		return creditRisk;
	}
	
	public void setCreditRisk(CreditRisk creditRisk) {
		this.creditRisk = creditRisk;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Double getCreditLimit() {
		return creditLimit;
	}
	
	public void setCreditLimit(Double creditLimit) {
		this.creditLimit = creditLimit;
	}
	
	public Integer getInterestRate() {
		return interestRate;
	}
		
}
