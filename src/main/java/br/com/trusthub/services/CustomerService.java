package br.com.trusthub.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.trusthub.domain.Customer;
import br.com.trusthub.domain.enums.CreditRisk;
import br.com.trusthub.dto.CustomerDTO;
import br.com.trusthub.repositories.CustomerRepository;
import br.com.trusthub.services.exception.ResourceNotFoundException;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	
	public Customer customerDTO(CustomerDTO customerDTO) {
		return new Customer(customerDTO.getName(), customerDTO.getCreditLimit(), CreditRisk.valueOf(customerDTO.getCreditRisk()));
	}
	
	public Customer createCustomer(Customer customer) {
		return customerRepository.save(customer);
	}
	
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}
	
	public Customer findById(Integer id) {
		return customerRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("customer " + id + " not found") );
	}
	
	public void updateCustomer(Customer customer) {
		this.findById(customer.getId());
		customerRepository.save(customer);
	}
	
	public void deleteCustomer(Integer id) {
		this.findById(id);
		customerRepository.deleteById(id);
	}
}
