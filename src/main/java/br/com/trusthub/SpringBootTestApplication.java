package br.com.trusthub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.trusthub.domain.Customer;
import br.com.trusthub.domain.enums.CreditRisk;
import br.com.trusthub.repositories.CustomerRepository;

@SpringBootApplication
public class SpringBootTestApplication implements CommandLineRunner{

	@Autowired
	private CustomerRepository customerRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootTestApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		Customer customer1 = new Customer("Customer1", 1000.00, CreditRisk.valueOf("A"));
		customerRepository.save(customer1);
		Customer customer2 = new Customer("Customer2", 1000.00, CreditRisk.valueOf("B"));
		customerRepository.save(customer2);
		Customer customer3 = new Customer("Customer3", 1000.00, CreditRisk.valueOf("C"));
		customerRepository.save(customer3);
	}
}
